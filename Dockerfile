FROM maven:3.6.3-jdk-11 AS builder

COPY ./src/ /root/src
COPY ./pom.xml /root/
COPY ./checkstyle.xml /root/
WORKDIR /root
RUN mvn package
RUN java -Djarmode=layertools -jar /root/target/ds2020_30245_pavel_alexandru_assigment_1_backend-1.0-SNAPSHOT.jar list
RUN java -Djarmode=layertools -jar /root/target/ds2020_30245_pavel_alexandru_assigment_1_backend-1.0-SNAPSHOT.jar extract
RUN ls -l /root

FROM openjdk:11.0.6-jre

ENV TZ=UTC
ENV DB_IP=ec2-54-220-229-215.eu-west-1.compute.amazonaws.com
ENV DB_PORT=5432
ENV DB_USER=fckitiioxotnam
ENV DB_PASSWORD=cca58dbcaa3c5ac1a65323ac16dc3134420831dac45f4ba17fa36ca82eaeea54
ENV DB_DBNAME=d9209utdg73aqb


COPY --from=builder /root/dependencies/ ./
COPY --from=builder /root/snapshot-dependencies/ ./

RUN sleep 10
COPY --from=builder /root/spring-boot-loader/ ./
COPY --from=builder /root/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher","-XX:+UseContainerSupport -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=1 -Xms512m -Xmx512m -XX:+UseG1GC -XX:+UseSerialGC -Xss512k -XX:MaxRAM=72m"]