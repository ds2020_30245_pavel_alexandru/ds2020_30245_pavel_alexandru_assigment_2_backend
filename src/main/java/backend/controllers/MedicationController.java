package backend.controllers;

import backend.dtos.MedicationDTO;
import backend.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicationDTO>> getMedications() {
        List<MedicationDTO> dtos = medicationService.findAll();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> getMedication(@PathVariable("id") UUID id) {

        MedicationDTO medicationDTO=medicationService.findById(id);
        return new ResponseEntity<>(medicationDTO, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertMedication(@Valid @RequestBody MedicationDTO medicationDTO) {

        UUID id = medicationService.insert(medicationDTO);
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deleteMedication(@PathVariable("id") UUID id) {

        medicationService.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<UUID> updateMedication(@PathVariable("id") UUID accountId,@Valid @RequestBody MedicationDTO medicationDTO) {

        medicationService.update(accountId,medicationDTO);
        return new ResponseEntity<>(accountId,HttpStatus.OK);
    }

    @GetMapping(value = "/name/{name}")
    public ResponseEntity<List<MedicationDTO>> getByName(@PathVariable("name") String name) {
        List<MedicationDTO> dtos = medicationService.findByName(name);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/sideeffects/{sideeffects}")
    public ResponseEntity<List<MedicationDTO>> getBySideeffects(@PathVariable("sideeffects") String sideeffects) {
        List<MedicationDTO> dtos = medicationService.findBySideEffects(sideeffects);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/dosage/{dosage}")
    public ResponseEntity<List<MedicationDTO>> getByDosage(@PathVariable("dosage") String dosage) {
        List<MedicationDTO> dtos = medicationService.findByDosage(dosage);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
}
