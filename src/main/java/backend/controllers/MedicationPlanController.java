package backend.controllers;

import backend.dtos.MedicationPlanDTO;
import backend.services.MedicationPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationPlan")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService) { this.medicationPlanService= medicationPlanService; }

    @PostMapping
    public ResponseEntity<UUID> insertMedicationPlan(@Valid @RequestBody MedicationPlanDTO medicationPlanDTO) {

        UUID id = medicationPlanService.insert(medicationPlanDTO);
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<List<MedicationPlanDTO>> getMedications(@PathVariable("id") UUID id) {
        List<MedicationPlanDTO> dtos = medicationPlanService.findByPatient(id);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}/{med}/{medTime}")
    public ResponseEntity<UUID> addMedication(@PathVariable("id") UUID id, @PathVariable("med") String med,@PathVariable("medTime") String medTime) {
        medicationPlanService.addMedication(id,med,medTime);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }

}
