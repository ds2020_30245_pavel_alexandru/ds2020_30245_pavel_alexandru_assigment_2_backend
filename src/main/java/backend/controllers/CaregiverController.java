package backend.controllers;

import backend.dtos.CaregiverDTO;
import backend.dtos.PatientDTO;
import backend.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping()
    public ResponseEntity<List<CaregiverDTO>> getCaregiver() {
        List<CaregiverDTO> dtos = caregiverService.findAll();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CaregiverDTO> getCaregiver(@PathVariable("id") UUID id) {

        CaregiverDTO caregiverDTO = caregiverService.findById(id);
        return new ResponseEntity<>(caregiverDTO, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertCaregiver(@Valid @RequestBody CaregiverDTO caregiverDTO) {

        UUID id = caregiverService.insert(caregiverDTO);
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deleteCaregiver(@PathVariable("id") UUID id) {

        caregiverService.delete(id);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<UUID> updateCaregiver(@PathVariable("id") UUID accountId,@Valid @RequestBody CaregiverDTO caregiverDTO) {

        caregiverService.update(accountId,caregiverDTO);
        return new ResponseEntity<>(accountId,HttpStatus.OK);
    }

    @GetMapping(value = "/name/{name}")
    public ResponseEntity<List<CaregiverDTO>> getByName(@PathVariable("name") String name) {
        List<CaregiverDTO> dtos = caregiverService.findByName(name);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/gender/{gender}")
    public ResponseEntity<List<CaregiverDTO>> getByGender(@PathVariable("gender") String gender) {
        List<CaregiverDTO> dtos = caregiverService.findByGender(gender);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/address/{address}")
    public ResponseEntity<List<CaregiverDTO>> getByAddress(@PathVariable("address") String address) {
        List<CaregiverDTO> dtos = caregiverService.findByAddress(address);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/bypatients/{patients}")
    public ResponseEntity<List<CaregiverDTO>> getByPatients(@PathVariable("patients")String patients) {
        List<CaregiverDTO> dtos = caregiverService.findByPatients(patients);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/dob/{dob}")
    public ResponseEntity<List<CaregiverDTO>> getByDob(@PathVariable("dob") String dob) {
        List<CaregiverDTO> dtos = caregiverService.findByDob(dob);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/patients/{id}")
    public ResponseEntity<ArrayList<PatientDTO>> getPatients(@PathVariable("id") UUID id) {

        ArrayList<PatientDTO> patientDTOS = caregiverService.getPatients(id);
        return new ResponseEntity<>(patientDTOS, HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}/{idPatient}")
    public ResponseEntity<UUID> addPatinet(@PathVariable("id") UUID id, @PathVariable("idPatient") UUID idPatient) {
        caregiverService.addPatient(id,idPatient);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }

    @DeleteMapping(value = "/update/{id}/{idPatient}")
    public ResponseEntity<UUID> deletePatient(@PathVariable("id") UUID id, @PathVariable("idPatient") UUID idPatient) {
        caregiverService.removePatient(id,idPatient);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }

}
