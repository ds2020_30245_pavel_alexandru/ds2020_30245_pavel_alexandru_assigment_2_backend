package backend.controllers;

import backend.dtos.MedicationPlanDTO;
import backend.dtos.PatientDTO;
import backend.services.CaregiverService;
import backend.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;
    private final CaregiverService caregiverService;

    @Autowired
    public PatientController(PatientService patientService,CaregiverService caregiverService) {
        this.patientService = patientService;
        this.caregiverService=caregiverService;
    }

    @GetMapping()
    public ResponseEntity<List<PatientDTO>> getPatients() {
        List<PatientDTO> dtos = patientService.findAll();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PatientDTO> getPatient(@PathVariable("id") UUID id) {

        PatientDTO patientDTO = patientService.findById(id);
        return new ResponseEntity<>(patientDTO, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertPatient(@Valid @RequestBody PatientDTO patientDTO) {

        UUID id = patientService.insert(patientDTO);
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deletePatient(@PathVariable("id") UUID id) {

        patientService.delete(id);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<UUID> updatePatient(@PathVariable("id") UUID accountId,@Valid @RequestBody PatientDTO patientDTO) {

        patientService.update(accountId,patientDTO);
        return new ResponseEntity<>(accountId,HttpStatus.OK);
    }

    @GetMapping(value = "/name/{name}")
    public ResponseEntity<List<PatientDTO>> getByName(@PathVariable("name") String name) {
        List<PatientDTO> dtos = patientService.findByName(name);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/gender/{gender}")
    public ResponseEntity<List<PatientDTO>> getByGender(@PathVariable("gender") String gender) {
        List<PatientDTO> dtos = patientService.findByGender(gender);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/address/{address}")
    public ResponseEntity<List<PatientDTO>> getByAddress(@PathVariable("address") String address) {
        List<PatientDTO> dtos = patientService.findByAddress(address);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/medicalrecord/{medRecord}")
    public ResponseEntity<List<PatientDTO>> getByMedicalRecord(@PathVariable("medRecord")String medRecord) {
        List<PatientDTO> dtos = patientService.findByMedRecord(medRecord);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/dob/{dob}")
    public ResponseEntity<List<PatientDTO>> getByDob(@PathVariable("dob") String dob) {
        List<PatientDTO> dtos = patientService.findByDob(dob);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/medicationplans/{id}")
    public ResponseEntity<ArrayList<MedicationPlanDTO>> getMedicationPlans(@PathVariable("id") UUID id) {

        ArrayList<MedicationPlanDTO> dtos = patientService.findMedicationPlans(id);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

}
