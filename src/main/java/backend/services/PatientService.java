package backend.services;

import backend.dtos.MedicationPlanDTO;
import backend.dtos.PatientDTO;
import backend.dtos.builders.MedicationPlanBuilder;
import backend.dtos.builders.PatientBuilder;
import backend.entities.MedicationPlan;
import backend.entities.Patient;
import backend.entities.Users;
import backend.repositories.MedicationPlanRepo;
import backend.repositories.PatientRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepo patientRepo;
    private final MedicationPlanRepo medicationPlanRepo;

    @Autowired
    public PatientService(PatientRepo patientRepo,MedicationPlanRepo medicationPlanRepo) {this.patientRepo= patientRepo;this.medicationPlanRepo=medicationPlanRepo;}

    public PatientDTO findById(UUID id) {

        Optional<Patient> patient=patientRepo.findById(id);
        if (!patient.isPresent()) {
            LOGGER.error("Patient not found.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPatientDTO(patient.get());
    }

    public ArrayList<PatientDTO> findByName(String name) {

        ArrayList<Patient> patients = patientRepo.findByName(name);
        if ( patients.isEmpty()) {
            LOGGER.error("Patients not found.");
        }
        return (ArrayList<PatientDTO>) patients.stream().map(PatientBuilder::toPatientDTO).collect(Collectors.toList());
    }

    public ArrayList<PatientDTO> findByDob(String dob) {

        Date dob1= null;
        try {
            dob1 = new SimpleDateFormat("yyyy-MM-dd").parse(dob);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ArrayList<Patient> patients = patientRepo.findByDob(dob1);
        if ( patients.isEmpty()) {
            LOGGER.error("Patients not found.");
        }
        return (ArrayList<PatientDTO>) patients.stream().map(PatientBuilder::toPatientDTO).collect(Collectors.toList());
    }

    public ArrayList<PatientDTO> findByGender(String gender) {

        ArrayList<Patient> patients = patientRepo.findByGender(gender);
        if ( patients.isEmpty()) {
            LOGGER.error("Patients not found.");
        }
        return (ArrayList<PatientDTO>) patients.stream().map(PatientBuilder::toPatientDTO).collect(Collectors.toList());
    }

    public ArrayList<PatientDTO> findByAddress(String address) {

        ArrayList<Patient> patients = patientRepo.findByAddress(address);
        if ( patients.isEmpty()) {
            LOGGER.error("Patients not found.");
        }
        return (ArrayList<PatientDTO>) patients.stream().map(PatientBuilder::toPatientDTO).collect(Collectors.toList());
    }

    public ArrayList<PatientDTO> findByMedRecord(String medRecord) {

        ArrayList<Patient> patients = patientRepo.findByMedRecord(medRecord);
        if ( patients.isEmpty()) {
            LOGGER.error("Patients not found.");
        }
        return (ArrayList<PatientDTO>) patients.stream().map(PatientBuilder::toPatientDTO).collect(Collectors.toList());
    }

    public ArrayList<PatientDTO> findAll()
    {
        ArrayList<Patient> patients = patientRepo.findAll();
        if ( patients.isEmpty()) {
            LOGGER.error("Patients not found.");
        }
        return (ArrayList<PatientDTO>) patients.stream().map(PatientBuilder::toPatientDTO).collect(Collectors.toList());
    }

    public UUID insert(PatientDTO patientDTO) {
        Patient patient = PatientBuilder.toEntity(patientDTO);
        patient = patientRepo.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public boolean delete(UUID id)
    {
        patientRepo.deleteById(id);
        return true;
    }

    public boolean update(UUID id,PatientDTO patientDTO)
    {
        Optional<Patient> patient = patientRepo.findById(id);
        if (!patient.isPresent()) {
            LOGGER.error("Patient not found.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + id);
        }

        Patient patient1 = patient.get();

        patient1.setName(patientDTO.getName());
        patient1.setDob(patientDTO.getDob());
        patient1.setGender(patientDTO.getGender());
        patient1.setAddress(patientDTO.getAddress());
        patient1.setMedRecord(patientDTO.getMedRecord());
        patientRepo.save(patient1);
        return true;
    }

    public ArrayList<MedicationPlanDTO> findMedicationPlans(UUID id) {

        ArrayList<MedicationPlan> medicationPlans = medicationPlanRepo.findByPatientId(id);
        if ( medicationPlans.isEmpty()) {
            LOGGER.error("No medication plans found for this patient.");
        }
        return (ArrayList<MedicationPlanDTO>) medicationPlans.stream().map(MedicationPlanBuilder::medicationPlanDTO).collect(Collectors.toList());
    }
}
