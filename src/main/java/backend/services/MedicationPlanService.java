package backend.services;

import backend.dtos.MedicationDTO;
import backend.dtos.MedicationPlanDTO;
import backend.dtos.builders.MedicationBuilder;
import backend.dtos.builders.MedicationPlanBuilder;
import backend.entities.Caregiver;
import backend.entities.Medication;
import backend.entities.MedicationPlan;
import backend.entities.Users;
import backend.repositories.MedicationPlanRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class MedicationPlanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanService.class);
    private final MedicationPlanRepo medicationPlanRepo;

    @Autowired
    public MedicationPlanService(MedicationPlanRepo medicationPlanRepo) {this.medicationPlanRepo=medicationPlanRepo;}

    public UUID insert(MedicationPlanDTO medicationPlanDTO) {
        MedicationPlan medicationPlan=MedicationPlanBuilder.toEntity(medicationPlanDTO);
        medicationPlan=medicationPlanRepo.save(medicationPlan);
        LOGGER.debug("Medication with id {} was inserted in db", medicationPlan.getId());
        return medicationPlan.getId();
    }

    public ArrayList<MedicationPlanDTO> findByPatient(UUID id) {

        ArrayList<MedicationPlan> medicationplans = medicationPlanRepo.findByPatientId(id);
        if ( medicationplans.isEmpty()) {
            LOGGER.error("Medication Plans not found.");
        }
        return (ArrayList<MedicationPlanDTO>) medicationplans.stream().map(MedicationPlanBuilder::medicationPlanDTO).collect(Collectors.toList());
    }

    public void addMedication(UUID id,String med,String medTime)
    {
        Optional<MedicationPlan> medicationPlan = medicationPlanRepo.findById(id);
        if (!medicationPlan.isPresent()) {
            LOGGER.error("Medication plan not found.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + id);
        }
        MedicationPlan medicationPlan1=medicationPlan.get();

        if(medicationPlan1.getMeds().isEmpty())
        {
            medicationPlan1.setMeds(med);
        }
        else
        {
            medicationPlan1.setMeds(medicationPlan1.getMeds()+","+med);
        }

        if(medicationPlan1.getMedsTime().isEmpty())
        {
            medicationPlan1.setMedsTime(medTime);
        }
        else
        {
            medicationPlan1.setMedsTime(medicationPlan1.getMedsTime()+","+medTime);
        }

        medicationPlanRepo.save(medicationPlan1);
    }
}
