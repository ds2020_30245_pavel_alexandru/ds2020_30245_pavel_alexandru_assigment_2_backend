package backend.services;

import backend.dtos.UserDTO;
import backend.dtos.builders.UserBuilder;
import backend.entities.Users;
import backend.repositories.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserRepo userRepo;

    @Autowired
    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public UserDTO findUser(String username,String password) {

        Optional<Users> user = userRepo.findByUsernameAndPassword(username,password);

        if (!user.isPresent()) {
            LOGGER.error("Invalid username.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with username: " + username);
        }

        return UserBuilder.toUserDTO(user.get());
    }

    public UUID insert(UserDTO userDTO) {
        Users user = UserBuilder.toEntity(userDTO);
        user = userRepo.save(user);
        LOGGER.debug("User with id {} was inserted in db", user.getId());
        return user.getId();
    }

    public boolean delete(UUID id)
    {
        userRepo.deleteById(id);
        return true;
    }

    public boolean update(UUID id, String username, String password)
    {
        Optional<Users> user = userRepo.findById(id);
        if (!user.isPresent()) {
            LOGGER.error("User not found.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + id);
        }

        Users user1=user.get();
        user1.setUserName(username);
        user1.setPassword(password);
        userRepo.save(user1);
        return true;
    }

    public boolean deleteByAccountId(UUID id)
    {
        userRepo.deleteByAccount(id);
        return true;
    }
}
