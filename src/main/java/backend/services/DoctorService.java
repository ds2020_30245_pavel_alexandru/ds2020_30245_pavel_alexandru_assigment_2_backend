package backend.services;

import backend.dtos.DoctorDTO;
import backend.dtos.builders.DoctorBuilder;
import backend.entities.Doctor;
import backend.entities.Users;
import backend.repositories.DoctorRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class DoctorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorService.class);
    private final DoctorRepo doctorRepo;

    @Autowired
    public DoctorService(DoctorRepo doctorRepo) {this.doctorRepo=doctorRepo;}

    public DoctorDTO findById(UUID id) {

        Optional<Doctor> doctor=doctorRepo.findById(id);
        if (!doctor.isPresent()) {
            LOGGER.error("Doctor not found.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + id);
        }
        return DoctorBuilder.toDoctorDTO(doctor.get());
    }

    public UUID insert(DoctorDTO doctorDTO) {
        Doctor doctor=DoctorBuilder.toEntity(doctorDTO);
        doctor = doctorRepo.save(doctor);
        LOGGER.debug("Doctor with id {} was inserted in db", doctor.getId());
        return doctor.getId();
    }

    public boolean delete(UUID id)
    {
        doctorRepo.deleteById(id);
        return true;
    }

    public boolean update(UUID id,DoctorDTO doctorDTO)
    {
        Optional<Doctor> doctor=doctorRepo.findById(id);
        if (!doctor.isPresent()) {
            LOGGER.error("Doctor not found.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + id);
        }

        Doctor doctor1=doctor.get();

        doctor1.setAddress(doctorDTO.getAddress());
        doctor1.setDob(doctorDTO.getDob());
        doctor1.setGender(doctorDTO.getGender());
        doctor1.setName(doctorDTO.getName());
        doctorRepo.save(doctor1);
        return true;
    }

}
