package backend.dtos;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class MedicationPlanDTO {

    private UUID id;
    private UUID patientId;
    private String meds;
    private String medsTime;
    private Date startDate;
    private Date endDate;


    public MedicationPlanDTO(UUID id,UUID patientId,String meds,String medsTime,Date startDate,Date endDate)
    {
        this.id=id;
        this.patientId=patientId;
        this.meds=meds;
        this.medsTime=medsTime;
        this.startDate=startDate;
        this.endDate=endDate;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getPatientId() {
        return patientId;
    }

    public void setPatientId(UUID patientId) {
        this.patientId = patientId;
    }


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getMedsTime() {
        return medsTime;
    }

    public void setMedsTime(String medsTime) {
        this.medsTime = medsTime;
    }

    public String getMeds() {
        return meds;
    }

    public void setMeds(String meds) {
        this.meds = meds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MedicationPlanDTO medicationPlanDTO = (MedicationPlanDTO) o;
        return Objects.equals(id, medicationPlanDTO.id) &&
                Objects.equals(patientId, medicationPlanDTO.patientId) &&
                Objects.equals(meds, medicationPlanDTO.meds) &&
                Objects.equals(medsTime, medicationPlanDTO.medsTime) &&
                Objects.equals(startDate, medicationPlanDTO.startDate) &&
                Objects.equals(endDate, medicationPlanDTO.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, patientId,meds,medsTime,startDate,endDate);
    }
}
