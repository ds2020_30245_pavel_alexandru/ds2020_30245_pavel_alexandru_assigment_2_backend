package backend.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;
import java.util.UUID;

public class UserDTO extends RepresentationModel<UserDTO> {

    private UUID id;
    private UUID linkId;
    private String userName;
    private String password;
    private String usertype;

    public UserDTO(UUID id,UUID linkId,String userName, String password, String usertype) {
        this.id = id;
        this.linkId=linkId;
        this.userName = userName;
        this.password = password;
        this.usertype = usertype;
    }

    public UUID getId() { return id; }

    public void setId(UUID id) { this.id = id; }

    public String getUserName() { return userName; }

    public void setUserName(String userName) { this.userName = userName; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public String getUsertype() { return usertype; }

    public void setUsertype(String usertype) { this.usertype = usertype; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserDTO user = (UserDTO) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(linkId, user.linkId) &&
                Objects.equals(userName, user.userName) &&
                Objects.equals(password, user.password) &&
                Objects.equals(usertype, user.usertype);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, linkId, userName, password, usertype);
    }

    public UUID getLinkId() {
        return linkId;
    }

    public void setLinkId(UUID linkId) {
        this.linkId = linkId;
    }
}
