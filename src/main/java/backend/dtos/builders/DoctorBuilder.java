package backend.dtos.builders;

import backend.dtos.DoctorDTO;
import backend.entities.Doctor;

public class DoctorBuilder {

    public static DoctorDTO toDoctorDTO(Doctor doctor) {
        return new DoctorDTO (doctor.getId(), doctor.getName(),doctor.getDob(), doctor.getGender(), doctor.getAddress());
    }

    public static Doctor toEntity(DoctorDTO doctorDTO)
    {
        return new Doctor(doctorDTO.getName(),doctorDTO.getDob(),doctorDTO.getGender(),doctorDTO.getAddress());
    }
}
