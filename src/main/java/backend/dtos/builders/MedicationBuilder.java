package backend.dtos.builders;

import backend.dtos.MedicationDTO;
import backend.entities.Medication;


public class MedicationBuilder {

    public static MedicationDTO medicationDTO(Medication medication) {
        return new MedicationDTO(medication.getId(),medication.getName(),medication.getSideEffects(),medication.getDosage());
    }

    public static Medication toEntity(MedicationDTO medicationDTO)
    {
        return new Medication(medicationDTO.getName(),medicationDTO.getSideEffects(),medicationDTO.getDosage());
    }
}
