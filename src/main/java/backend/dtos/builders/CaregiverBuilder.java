package backend.dtos.builders;

import backend.dtos.CaregiverDTO;
import backend.entities.Caregiver;

public class CaregiverBuilder {

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver) {
        return new CaregiverDTO (caregiver.getId(), caregiver.getName(),caregiver.getDob(), caregiver.getGender(), caregiver.getAddress(), caregiver.getPatients());
    }

    public static Caregiver toEntity(CaregiverDTO caregiverDTO)
    {
        return new Caregiver(caregiverDTO.getName(),caregiverDTO.getDob(),caregiverDTO.getGender(),caregiverDTO.getAddress(),caregiverDTO.getPatients());
    }
}
