package backend.dtos;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class DoctorDTO {

    private UUID id;
    private String name;
    private Date dob;
    private String gender;
    private String address;

    public DoctorDTO (UUID id, String name, Date dob, String gender, String address)
    {
        this.id=id;
        this.name=name;
        this.dob=dob;
        this.gender=gender;
        this.address=address;
    }

    public UUID getId(){return id;}

    public void setId(UUID id){this.id=id;}

    public String getName(){return name;}

    public void setName(String name){this.name=name;}

    public Date getDob(){return  dob; }

    public void setDob(Date dob){this.dob=dob;}

    public String getGender(){return gender;}

    public void setGender(String gender){this.gender=gender;}

    public String getAddress(){return address;}

    public void setAddress(String address){this.address=address;}


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DoctorDTO doctorDTO = (DoctorDTO) o;
        return Objects.equals(id, doctorDTO.id) &&
                Objects.equals(name, doctorDTO.name) &&
                Objects.equals(dob, doctorDTO.dob) &&
                Objects.equals(gender, doctorDTO.gender) &&
                Objects.equals(address, doctorDTO.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, name,dob,gender,address);
    }
}
