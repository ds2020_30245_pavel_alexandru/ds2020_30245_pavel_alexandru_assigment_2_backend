package backend.repositories;

import backend.entities.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public interface MedicationPlanRepo extends JpaRepository<MedicationPlan, UUID> {

   ArrayList<MedicationPlan> findByPatientId(UUID patientID);

    <S extends MedicationPlan> S save(S s);

}
