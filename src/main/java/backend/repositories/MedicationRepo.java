package backend.repositories;

import backend.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

public interface MedicationRepo extends JpaRepository<Medication, UUID> {

    Optional<Medication> findById(UUID id);

    ArrayList<Medication> findAll();

    ArrayList<Medication>  findByName(String name);

    ArrayList<Medication>  findBySideEffects(String sideEffects);

    ArrayList<Medication> findByDosage(String dosage);

    <S extends Medication> S save(S s);

    void deleteById(UUID id);
}
