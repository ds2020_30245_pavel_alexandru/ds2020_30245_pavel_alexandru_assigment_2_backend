package backend.repositories;

import backend.entities.Activities;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ActivityRepo extends JpaRepository<Activities, UUID> {

}
